# Python stuff

Basic example of using an `Abstract` class in Python.

## Executing
To run this example, simply execute the script with python

```bash
python run.py
```

The output should look like the following:

```text
==================
Let's here what [Winston] has to say
I don't like cafeteria food
==================
==================
Let's here what [Finley] has to say
WHY MUST I TAKE SO MANY CLASSES
==================
==================
Let's here what [Penelope] has to say
The struggle is real... but I'm be working soon!
==================
```

## Explanation

### Define Abstract Class
The first step was to define an `Absract` class with python. This file is in
[human/human.py](https://gitlab.com/john-s-flores/pymisc/blob/master/human/human.py#L4)

The main takeaway from this is the need to define an `@abstractmethod` and properly use the ABCMeta
object. This can be done with the `@abstractmethod` annotation and `ABCMeta` (if using Python 2). 
The `ABCMeta` acts like a mixin to add functionality. You can read more about [here](https://docs.python.org/2/library/abc.html#abc.ABCMeta.register)


```python
from abc import ABCMeta, abstractmethod

class AbstractHuman:
    __metaclass__ = ABCMeta

    def __init__(self, name):
        self.name = name

    # Abstract method that will implemented by subclasses
    # otherwise an error will occur.
    @abstractmethod
    def complain(self):
        pass

```

Given this definition, all subclasses wanting to use this Abstract class **must
implement** this method. 

*Side Note*: Using abstract classes can be used to implement a [`Template Method Pattern`](https://en.wikipedia.org/wiki/Template_method_pattern)
which can lead to some useful design solutions. Note, not all languages have abstract classes.

### Use Abstract Class
The next step is to create a subclass that inherits from that Abstract class. I
included a couple of examples of these in the `college.` and `student/` directories.
But to use on as an example, 

```python
from human.human import AbstractHuman

class FreshmanStudent (AbstractHuman):
    
    def __init__(self, name):
        self.name = name

    def complain(self):
        return "WHY MUST I TAKE SO MANY CLASSES"

```

Here, we must import the `AbstractHuman` class from the `human` package. Then,
we create the `FreshmanStudent` and have it inherit the `AbstractHuman` class. 

Since `complain()` was defined as an abstract method in our Abstract class, this
sublass needs to provide an implementation. 

If you don't implement that method, you will get an error like:

```error
TypeError: Can't instantiate abstract class FreshmanStudent with abstract methods complain
```

**Note**: Abstract classes cannot be directly instantiated. They can only be instantiated
by sublasses inheriting from it.

In terms of importing, you could have also done it this way:
```python
from human import human

class FreshmanStudent (human.AbstractHuman):
```

The driver script is `run.py` which has the following: