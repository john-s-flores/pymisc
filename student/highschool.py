# Another way of importing
from human import human

class HighschoolStudent (human.AbstractHuman):
    
    def __init__(self, name):
        self.name = name

    def complain(self):
        return "I don't like cafeteria food"
