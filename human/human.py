# https://docs.python.org/2/library/abc.html#abc.ABCMeta.register
from abc import ABCMeta, abstractmethod

class AbstractHuman:
    __metaclass__ = ABCMeta

    def __init__(self, name):
        self.name = name

    # Abstract method that will implemented by subclasses
    # otherwise an error will occur.
    @abstractmethod
    def complain(self):
        pass

    # Note that this is just a basic class
    # method
    def speak (self):
        print("==================")
        # This is logic from this abstract class
        msg = "Let's here what [{0}] has to say".format(self.name)
        print(msg)

        # Now we delegate function call to subclass that implemented
        # the `complain()` function.
        content = self.complain()

        # Print the stuff
        print(content)
        print("==================")
