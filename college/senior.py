from human.human import AbstractHuman

class SeniorStudent (AbstractHuman):
    
    def __init__(self, name):
        self.name = name

    def complain(self):
        return "The struggle is real... but I'm be working soon!"
