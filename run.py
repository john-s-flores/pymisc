from student.highschool import HighschoolStudent
from college.freshman import FreshmanStudent
from college.senior import SeniorStudent


# instantiate instances
hs_student = HighschoolStudent("Winston")
freshman_student = FreshmanStudent("Finley")
senior_student = SeniorStudent("Penelope")


# Start complaining
spacer = "====================="

hs_student.speak()
freshman_student.speak()
senior_student.speak()
